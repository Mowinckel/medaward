<?php include_once('php/helper.php'); ?>
<?php
    if(!isset($_COOKIE["logged_in"]))
    {
        header("Location: " . "login.php");
        die();
    }
    $event_id = 1; // this can be easily changed from just hardcoding which id to be used, to instead grab the active one
    // i just didn't trust whoever would admin this would handle active/inactive...I'm a bitch, deal with it
    $categories = array();
    $event = grabEvent(1);
    $categories = grabCategories(1);
    //$nominees = array();
    /*
    foreach ($categories as $key => $category) {
        $nominees[$category['id']] = grabNominees($category['id']);
    }
    */
?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>MED-Award Voting</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,500,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
</head>

<body>
    <div class="container-fluid text-center statistics">
        <!--
        <select name="events" id="events">
            <option value="0">-Select an event-</option>
        </select>
        -->

        <select name="categories" id="categories" style="padding: .5em; margin: 1em 0;">
            <option value="0">-Select a category-</option>
            <?php foreach($categories as $category) : ?>
                <option value="<?php echo $category['id']; ?>">
                    <?php echo $category['name']; ?>
                </option>
            <?php endforeach; ?>
        </select>
        <div id="container" style="width:100%; height:400px;"></div>
        <div class="col col-xs-12 text-center">
            <div class="col col-xs-4 col-xs-offset-4">
                <a disabled id="change_voting_status" class="btn btn-primary go-button gold-gradient" role="button">Just do it</a>
            </div>
        </div>
    </div>





    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/highcharts.js"></script>

    <script type="text/javascript">
    var event_id = <?php echo $event['id']; ?>;
    var event_name = '<?php echo $event['name']; ?>';
    function handleCharts(parameters)
    {
         $('#container').highcharts({
            chart: {
                type: 'column'               
            },
            
            title: {
                text: parameters['name'],
                style: {
                    color:'#555555',
                    fontSize: '30px'
                }
            },
            xAxis: {
                type:"category",
                lineColor:'#333333',
                tickColor: '#333333',
                gridLineColor: '#333333'
            },
            yAxis: {
                title: {
                    text: 'Number of votes'
                },
                lineColor:'#333333',
                gridLineColor: '#333333'
            },
            legend: {
                enabled:false
            },
            plotOptions: {
                series: {
                    
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        style: {
                            textShadow: false,
                            color: '#555555'
                        }
                    }
                },
                column: {
                    colorByPoint: true
                }
            },
            credits: {
                enabled:false
            },
            colors: ['#ca9d32', '#49d39f', '#7b1697'],
            series: [{
                data: parameters['data'],

            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
        });
    }
    $(document).ready(function()
    {
        $("#categories").on("change", function()
        {
            var category_id = $(this).val();
            if(category_id < 1)
            {
                return false;
            }

            var category_name = $("#categories option:selected").text();
            $.ajax({
                method: "POST",
                url: "php/check_category.php",
                data: {category_id},
                success: function(data)
                {
                    console.log("data : " + data);
                    data = jQuery.parseJSON(data);
                    console.log("I DID IT!");
                    console.log(data);
                    if(!data["voting"])
                    {
                        $("#change_voting_status").text("Start Vote");
                    }
                    else
                    {
                        $("#change_voting_status").text("Stop Vote");
                    }
                }
            });
            $.ajax({
                method: "POST",
                url: "php/get_nominees.php",
                data :{category_id: category_id},
                success: function(data)
                {
                    $("#change_voting_status").attr("disabled", false);
                    console.log(data);
                    var parameters = [];
                    parameters['name'] = category_name;
                    parameters['data'] = jQuery.parseJSON(data);

                    console.log(parameters);

                    handleCharts(parameters);
                    console.log("success " + data);
                },
                error : function(data)
                {
                    console.log("ERROR " + data);
                }
            });
        });
        $("#change_voting_status").on("click", function()
        {
            var category_id = $("#categories").val();
            var nominee_id = $(this).attr("id");
            $.ajax({
              method: "POST",
              url: "php/change_voting_status.php",
              data: { category_id:category_id},
              success: function(data) {
                  console.log("success " + data);
                  data = jQuery.parseJSON(data);
                  if(data['voting'] > 0)
                    {
                        $("#change_voting_status").text("Stop Vote");
                    }
                    else
                    {
                        $("#change_voting_status").text("Start Vote");
                    }
              },
              error: function(data) {
                  console.log("error " + data);
              }
            });
        });
        handleCharts([]);
    });
    </script>
</body>

</html>