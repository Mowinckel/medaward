<?php include_once('php/helper.php'); ?>
<?php
    $event_id = 1; // this can be easily changed from just hardcoding which id to be used, to instead grab the active one
    // i just didn't trust whoever would admin this would handle active/inactive...I'm a bitch, deal with it
    $categories = array();
    $event = grabEvent(1);
    $categories = grabCategories(1);
    $nominees = array();
    foreach ($categories as $key => $category) {
        $nominees[$category['id']] = grabNominees($category['id']);
    }
?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>MED-Award Voting</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,500,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="description" content="Voting website for MedAward">
    <meta name="keywords" content="Medialogy, Voting, Awesomeness">
    <meta name="author" content="MedAcademy">
</head>
<body>
    <div class="container-fluid text-center">
        <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="myModal">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <p> </p>
              <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
              <button type="button" class="btn btn-primary" id="btnConfirm">Yes</button>
            </div>
          </div>
        </div>
        <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel2" id="warning">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <p style="color:#BA423F">No voting yet, you rapscallion!</p>
              <img src="img/nono.gif" style="padding: 2em;">
            </div>
          </div>
        </div>
        <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel3" id="alreadyVoted">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <p style="color:#BA423F">You already gave your vote ;)</p>
            </div>
          </div>
        </div>
        <section class="row introduction">
            <article>
                <div class="col col-xs-12">
                    <img src="img/Logo_watermark_noalpha.png" />
                </div>
                <div class="col col-xs-12">
                    <h1>MED-Award</h1>
                    <h2>Spring 2016</h2>
                </div>
                <div class="col col-xs-12">
                    <p>
                    <em>Welcome to MED-Award Spring 2016!</em>
                    <br />
                    This website will show you the nominees in each category and let you vote on each by <br /> <b>clicking the image</b>. 
                    </p>
                    <p class="tip">You can expand each category to see and vote for nominees.</p>
                </div>
            </article>
        </section>
        <section class="row nominees">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <?php foreach ($categories as $key => $category) :?>
                    <div class="panel panel-default">
                        <div class="panel-heading gold-gradient" role="tab" id="heading<?php echo $category['id']; ?>">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $category['id']; ?>" aria-expanded="false" aria-controls="collapse<?php echo $category['id']; ?>">
                                <?php echo $category['name']; ?>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse<?php echo $category['id']; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $category['id']; ?>">
                            <?php foreach ($nominees[$category['id']] as $key => $nominee) : ?>
                                
                                    <div id="<?php echo $nominee['id']; ?>" class="panel-body nominee <?php echo $category['id']; ?>">
                                        <img src="<?php echo $nominee['image']; ?>">
                                        <h1><?php echo $nominee['name'] ?></h1>
                                        <!-- <h2><?php echo $nominee['group_name'] ?></h2> -->
                                    </div>
                                
                            <?php endforeach; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </section>
    </div>





    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script>
    $(document).ready(function()
    {
        $(".nominee").on("click", function()
        {
            var $nominee = $(this);
            var category_id = $(this).attr('class').split(' ')[2];
            if(document.cookie.indexOf("voted"+category_id) >= 0)
            {
                $('#alreadyVoted').modal('show');
                return false;
            }
            var voting = false;
            $.ajax({
                method: "POST",
                url: "php/check_category.php",
                data: {category_id},
                success: function(data)
                {
                    console.log(category_id);
                    console.log("success");
                    console.log(data);
                    data = jQuery.parseJSON(data);
                    if(data["voting"])
                    {
                        voting = true;
                    }
                    else
                    {
                        $('#warning').modal('show');
                        return false;
                        console.log("not voted");
                    }
                    $('#myModal').modal('show');
                    var nominee_name = $nominee.find("h1:first").text();
                    $("#myModal").find("p").text("Are you sure you want to vote for " + nominee_name + " ");
                    var nominee_id = $nominee.attr("id");

                    $("#btnConfirm").on("click",  function() {
                    
                        $.ajax({
                          method: "POST",
                          url: "php/vote_nominee.php",
                          data: { nominee_id:nominee_id, category_id: category_id},
                          success: function(data) {
                            data = jQuery.parseJSON(data);
                            if(data["voted"])
                            {
                                location.reload();
                                console.log("voted");
                            }
                            else
                            {
                                console.log("not voted");
                            }
                          },
                          error: function(data) {
                              console.log("error" + data);
                          }
                        });
                    });
                },
                error: function(data)
                {
                    console.log("success");
                    console.log(data);
                }
            });
        });
    });
    </script>
</body>

</html>