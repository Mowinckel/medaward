<?php
    if(isset($_COOKIE['logged_in']))
    {
        // depending on the server, you might need to add $_SERVER['SERVER_NAME']
        header("Location: " . "statistic.php");
        die();
    }
?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>MED-Award Voting</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,500,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
</head>

<body>
    <div class="container-fluid text-center">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <h1 class="text-center login-title">Admin sign in</h1>
                    <div class="account-wall">
                        <?php // dammit mark, don't forget to put names to fields anymore, i was trying to see why i was getting no input for ages...kisses tho ?>
                        <form class="form-signin" action="php/login.php" method="POST">
                        <input name="username" id="username" type="text" class="form-control" placeholder="Email" required autofocus>
                        <input name="password" id="password" type="password" class="form-control" placeholder="Password" required>
                        <button class="btn btn-lg btn-primary btn-block gold-gradient" type="submit">
                            Sign in</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>





    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <?php if(isset($_COOKIE["failed_login"])) : ?>
        <script>
            alert("failed to log in");
        </script>
    <?php 
        setcookie('failed_login', null, -1, '/');
        unset($_COOKIE['failed_login']);
    ?>
    <?php endif; ?>
</body>

</html>