<?php
require "resources.php";

	// get the raw POST data
    $rawData = $_POST;
    // this returns null if not valid json
	$password = encrypt($rawData['password']);
	$username = $rawData['username'];
	$conn = null;
	$conn = mysqlConnect();
	$query = "SELECT username FROM users WHERE username = '{$username}'
				 AND password = '{$password}'";
	$result = $conn->query($query);
	$result_arr = array();
	$response = FALSE;
	if(mysqli_num_rows($result) > 0)
	{
		$response = TRUE;
	}
	if($response)
	{
		if(!isset($_COOKIE['logged_in']))
		{
			// 86400 means 24 hours
			setcookie("logged_in", TRUE, time() + (86400 * 30), "/");
		}
		header("Location: " . "../statistic.php");
		die();
	}
	else
	{
		setcookie("failed_login", TRUE, time() + (5400 * 30), "/");
		header("Location: " . "../login.php");
		die();
	}
	return;
?>