<?php
	require "resources.php";

	// get the raw POST data
    $rawData = $_POST;
    // this returns null if not valid json
	$category_id = $rawData['category_id'];
	$conn = null;
	$conn = mysqlConnect();
	$query = "SELECT voting FROM categories WHERE id = {$category_id};";
	$result = $conn->query($query);
	$voting = 0;
	if(mysqli_num_rows($result) > 0)
	{
		while($row = $result->fetch_array(MYSQLI_ASSOC))
		{
			$voting = $row["voting"];
		}
		$result = null;
		if($voting == 0)
		{
			$voting = 1;
		}
		else
		{
			$voting = 0;
		}
		$query = "UPDATE categories SET voting = {$voting}
				  WHERE id = {$category_id}";
		$result = $conn->query($query);
		if($result)
		{
			$result_array = ["voting" => $voting];
			echo json_encode($result_array);
		}else
		{
			$result_array = ["voting" => false];
			echo json_encode($result_array);
		}
		
	}

	return;
?>