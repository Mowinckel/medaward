CREATE SCHEMA `medawards`;

#ALTER TABLE `medawards`.`events`
#ADD COLUMN `created_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;

CREATE TABLE `medawards`.`events` (
	`id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(75) NULL,
    `created_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(`id`));

ALTER TABLE `events` ADD `voting` TINYINT NOT NULL DEFAULT '0' AFTER `created_time`;

CREATE TABLE `medawards`.`categories` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `event_id` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY(`event_id`) REFERENCES `events`(`id`));

ALTER TABLE `categories` ADD `voting` TINYINT NOT NULL DEFAULT '0' AFTER `name`;

CREATE TABLE `medawards`.`nominees` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `category_id` INT NULL,
  `name` VARCHAR(45) NULL,
  'group_name' VARCHAR(45) NULL,
  'image' TEXT NULL,
  `votes` INT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY(`category_id`) REFERENCES `categories`(`id`));

CREATE TABLE `medawards`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NULL,
  `password` TEXT NULL,
  PRIMARY KEY (`id`));

INSERT INTO `medawards`.`events` (`name`) VALUES ('Spring 2016');
  
INSERT INTO `medawards`.`categories` (`event_id`, `name`) VALUES ('1', 'Best AV');
INSERT INTO `medawards`.`categories` (`event_id`, `name`) VALUES ('1', 'Best Poop');
INSERT INTO `medawards`.`categories` (`event_id`, `name`) VALUES ('1', 'Best Lil Child');
INSERT INTO `medawards`.`categories` (`event_id`, `name`) VALUES ('1', 'Best Poop');

INSERT INTO `medawards`.`nominees` (`category_id`, `name`, `group_name`, `image`, `votes`) VALUES ('1', 'Mark', 'poop', 'img/Logo_watermark_noalpha.png', '2');
INSERT INTO `medawards`.`nominees` (`category_id`, `name`, `group_name`, `image`, `votes`) VALUES ('1', 'Ted', 'poop', 'img/Logo_watermark_noalpha.png', '22');
INSERT INTO `medawards`.`nominees` (`category_id`, `name`, `group_name`, `image`, `votes`) VALUES ('1', 'Imran', 'poop', 'img/Logo_watermark_noalpha.png', '23');
INSERT INTO `medawards`.`nominees` (`category_id`, `name`, `group_name`, `image`, `votes`) VALUES ('2', 'George', 'poop', 'img/Logo_watermark_noalpha.png', '1');
INSERT INTO `medawards`.`nominees` (`category_id`, `name`, `group_name`, `image`, `votes`) VALUES ('2', 'Mikkel', 'poop', 'img/Logo_watermark_noalpha.png', '2');
INSERT INTO `medawards`.`nominees` (`category_id`, `name`, `group_name`, `image`, `votes`) VALUES ('3', 'Jeremy', 'poop', 'img/Logo_watermark_noalpha.png', '3');
INSERT INTO `medawards`.`nominees` (`category_id`, `name`, `group_name`, `image`, `votes`) VALUES ('4', 'Antonio', 'poop', 'img/Logo_watermark_noalpha.png', '60');
INSERT INTO `medawards`.`nominees` (`category_id`, `name`, `group_name`, `image`, `votes`) VALUES ('4', 'Wolverine', 'poop', 'img/Logo_watermark_noalpha.png', '29');

INSERT INTO `medawards`.`users` (`username`, `password`) VALUES ('admin', '4bcbdfc5de71944f887de064cbd1154d');