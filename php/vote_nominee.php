<?php
	require "resources.php";

	// get the raw POST data
    $rawData = $_POST;
    // this returns null if not valid json
	$nominee_id = $rawData['nominee_id'];
	$category_id = $rawData['category_id'];
	$result_array = ["voted" => false];
	if(isset($_COOKIE["voted{$category_id}"]))
	{
		echo json_encode($result_array);
		return;
	}
	$conn = null;
	$conn = mysqlConnect();
	$query = "SELECT voting FROM categories WHERE id = {$category_id};";

	$result = $conn->query($query);
	$voting = 0;
	
	if(mysqli_num_rows($result) > 0)
	{
		while($row = $result->fetch_array(MYSQLI_ASSOC))
		{
			$voting = $row["voting"];
			if($voting > 0)
			{
				$query = "UPDATE nominees SET votes = votes+1
				  WHERE id = {$nominee_id}";
				$result = $conn->query($query);
				if($result)
				{
					// 259200 is 72 hours , thought it should be enough
					setcookie("voted{$category_id}", TRUE, time() + (259200 * 30), "/");
					$result_array = ["voted" => true];
				}
			}
			echo json_encode($result_array);
			return;
		}
	}
	echo json_encode($result_array);
	return;
?>