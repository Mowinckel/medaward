<?php
	require "resources.php";

	// get the raw POST data
    $rawData = $_POST;
    // this returns null if not valid json
	$category_id = $rawData['category_id'];
	$result_array = ["voting" => false];
	$conn = null;
	$conn = mysqlConnect();
	$query = "SELECT voting FROM categories WHERE id = {$category_id};";

	$result = $conn->query($query);
	$voting = 0;
	
	if(mysqli_num_rows($result) > 0)
	{
		while($row = $result->fetch_array(MYSQLI_ASSOC))
		{
			$voting = $row["voting"];
			if($voting > 0)
			{
				$result_array = ["voting" => true];
			}
			echo json_encode($result_array);
			return;
		}
	}
	echo json_encode($result_array);
	return;
?>