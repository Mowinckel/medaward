<?php
	require "resources.php";

	function grabEvents()
	{
		$conn = null;
		$conn = mysqlConnect();
		$query = "SELECT * FROM events";
		$result = $conn->query($query);
		$result_arr = array();
		if(mysqli_num_rows($result) > 0)
		{
			while($row = $result->fetch_array(MYSQLI_ASSOC))
			{
				$temp = array("id" => $row['id'],
							  "name" => $row['name'],
							  "created_time" => $row['created_time'],
							  "voting" => $row['voting']);
				array_push($result_arr, $temp);
			}
		}
		return $result_arr;
	}

	function grabEvent($id)
	{
		$conn = null;
		$conn = mysqlConnect();
		$query = "SELECT * FROM events where id = {$id}";
		$result = $conn->query($query);
		$result_arr = array();
		if(mysqli_num_rows($result) > 0)
		{
			while($row = $result->fetch_array(MYSQLI_ASSOC))
			{
				$result_arr = $row;
			}
		}
		return $result_arr;
	}

	function grabCategories($event_id)
	{
		$conn = null;
		$conn = mysqlConnect();
		$query = "SELECT * FROM categories WHERE event_id = {$event_id}";
		$result = $conn->query($query);
		$result_arr = array();
		if(mysqli_num_rows($result) > 0)
		{
			while($row = $result->fetch_array(MYSQLI_ASSOC))
			{
				$temp = array("id" => $row['id'],
							  "event_id" => $row['event_id'],
							  "name" => $row['name']);
				array_push($result_arr, $temp);
			}
		}
		return $result_arr;
	}
	function grabNominees($category_id)
	{
		$conn = null;
		$conn = mysqlConnect();
		$query = "SELECT * FROM nominees WHERE category_id = {$category_id}";
		$result = $conn->query($query);
		$result_arr = array();
		if(mysqli_num_rows($result) > 0)
		{
			while($row = $result->fetch_array(MYSQLI_ASSOC))
			{
				$temp = array("id" => $row['id'],
							  "category_id" => $row['category_id'],
							  "name" => $row['name'],
							  "group_name" => $row['group_name'],
							  "image" => $row['image'],
							  "votes" => $row['votes']);
				array_push($result_arr, $temp);
			}
		}
		return $result_arr;
	}
?>