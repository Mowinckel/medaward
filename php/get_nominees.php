<?php 
	require "resources.php";

	// get the raw POST data
    $rawData = $_POST;
    // this returns null if not valid json
	$category_id = $rawData['category_id'];
	$conn = null;
	$conn = mysqlConnect();
	$query = "SELECT * FROM nominees WHERE category_id = {$category_id}";
	$result = $conn->query($query);
	$result_arr = array();
	if(mysqli_num_rows($result) > 0)
	{
		while($row = $result->fetch_array(MYSQLI_ASSOC))
		{
			$temp = array($row['name'], (int)$row['votes']);
			array_push($result_arr, $temp);
		}

	}

	echo json_encode($result_arr);
	return;
?>